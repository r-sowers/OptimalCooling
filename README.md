Code for Manuel D, Sowers RB. Optimal transport to cold chain in perishable hand-picked agriculture. Natural Resource Modeling. 2017;30:e12124. https://doi.org/10.1111/nrm.12124

Project director:
Richard Sowers <r-sowers@illinois.edu>, website
<http://publish.illinois.edu/r-sowers/>
Contributor:  Devasia Manuel

Main file is OptimalCooling.ipynb